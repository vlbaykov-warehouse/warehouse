package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import feign.FeignException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.rating.RatingServiceClient;

import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@EnableConfigurationProperties(WarehouseProperties.class)
@WireMockTest(httpPort = 8082)
class RatingServiceClientTests {

    static String ratingEndpoint = "/api/v1/rating/product/{productId}";
    static String emptyResponse = TestUtils.getJsonResource("empty_response.json");
    static String ratingNotFoundResponse = TestUtils.getJsonResource("rating_not_found_response.json");
    static String ratingInvalidProductIdResponse = TestUtils.getJsonResource("rating_invalid_product_id_response.json");

    @Autowired
    WarehouseProperties properties;

    @Autowired
    RatingServiceClient ratingServiceClient;

    @Test
    @DisplayName("Request rating by product id - success")
    void testSuccessfulRequestRatingByProductId() {
        String url = ratingEndpoint.replace("{productId}", String.valueOf(1));
        stubFor(post(urlEqualTo(url)).willReturn(okJson(emptyResponse)));

        ResponseDto<?> response = ratingServiceClient.requestRatingByProductId(1);
        verify(postRequestedFor(urlEqualTo(url)));
        assertNotNull(response);
        assertNull(response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("failedRequestRatingByProductIdArguments")
    @DisplayName("Request rating by product id - fail")
    void testFailedRequestRatingByProductId(
            String url,
            long productId,
            ResponseDefinitionBuilder response,
            Class<Exception> expectedExceptionType,
            String expectedMessage
    ) {
        stubFor(post(urlEqualTo(url)).willReturn(response));
        Exception exception = assertThrows(expectedExceptionType, () -> ratingServiceClient.requestRatingByProductId(productId));
        assertEquals(expectedMessage, exception.getMessage());
    }

    static Stream<Arguments> failedRequestRatingByProductIdArguments() {
        return Stream.of(
                Arguments.of(
                        ratingEndpoint.replace("{productId}", String.valueOf(2)),
                        2,
                        notFound().withBody(ratingNotFoundResponse.replace("{0}", String.valueOf(2))),
                        FeignException.class,
                        String.format(
                                "[404 Not Found] during [POST] to [http://localhost:8082/api/v1/rating/product/2] [RatingServiceClient#requestRatingByProductId(long)]: [%s]",
                                ratingNotFoundResponse.replace("{0}", String.valueOf(2)))
                ),
                Arguments.of(
                        ratingEndpoint.replace("{productId}", String.valueOf(Integer.MIN_VALUE)),
                        Integer.MIN_VALUE,
                        badRequest().withBody(ratingInvalidProductIdResponse.replace("{0}", String.valueOf(Integer.MIN_VALUE))),
                        FeignException.class,
                        String.format(
                                "[400 Bad Request] during [POST] to [http://localhost:8082/api/v1/rating/product/-2147483648] [RatingServiceClient#requestRatingByProductId(long)]: [%s]",
                                ratingInvalidProductIdResponse.replace("{0}", String.valueOf(Integer.MIN_VALUE))
                        )
                )
        );
    }

}
