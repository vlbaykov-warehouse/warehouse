package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client.ProductStockClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockFindResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceDto;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductStockTests extends WarehouseApplicationTests {

    static int defaultPageSize = 10;

    @Autowired
    ProductStockClient productStockClient;

    @Test
    @DisplayName("Put product in stock - success")
    void testSuccessfulPutProductInStock() {
        ResponseDto<ProductStockDto> response = productStockClient.putProductInStock(warehousePlaceA1.getId(), product1.getId(), free(warehousePlaceA1));
        ProductStockDto expectedResult = productStock(product1, withChangedOccupied(warehousePlaceA1, free(warehousePlaceA1)), free(warehousePlaceA1));
        assertNotNull(response);
        assertEquals(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("failedPutProductInStockArguments")
    @DisplayName("Put product in stock - fail")
    void testFailedPutProductInStock(
            Object productId,
            Object warehousePlaceId,
            Object quantity,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<?> response = productStockClient.putProductInStock(warehousePlaceId, productId, quantity);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedPutProductInStockArguments() {
        return Stream.of(
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA1.getId(),
                        0,
                        error(HttpStatus.BAD_REQUEST, "Product quantity should be more than 0")
                ),
                Arguments.of(
                        product1.getId(),
                        999,
                        free(warehousePlaceA1),
                        error(HttpStatus.NOT_FOUND, "Warehouse place with id [999] not found")
                ),
                Arguments.of(
                        999,
                        warehousePlaceA1.getId(),
                        free(warehousePlaceA1),
                        error(HttpStatus.NOT_FOUND, "Product with id [999] not found")
                ),
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA1.getId(),
                        999,
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not enough space to store [%d] pcs of product with id [%d]. " +
                                        "It may store not more than [%d] pcs of product.",
                                warehousePlaceA1.getId(),
                                999,
                                product1.getId(),
                                free(warehousePlaceA1)))
                ),
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA1.getId(),
                        warehousePlaceA1.getCapacity(),
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not enough space to store [%d] pcs of product with id [%d]. " +
                                        "It may store not more than [%d] pcs of product.",
                                warehousePlaceA1.getId(),
                                warehousePlaceA1.getCapacity(),
                                product1.getId(),
                                free(warehousePlaceA1)))
                )
        );
    }

    @ParameterizedTest
    @MethodSource("successfulTakeProductFromStockArguments")
    @DisplayName("Take product from stock - success")
    void testSuccessfulTakeProductFromStock(
            Object productId,
            Object warehousePlaceId,
            Object quantity,
            ProductStockDto expectedResult
    ) {
        ResponseDto<?> response = productStockClient.takeProductFromStock(warehousePlaceId, productId, quantity);
        assertNotNull(response);
        assertEquals(expectedResult, response.getResult());
    }

    private Stream<Arguments> successfulTakeProductFromStockArguments() {
        return Stream.of(
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA2.getId(),
                        productStock1A2.getQuantity() / 2,
                        productStock(
                                product1,
                                withChangedOccupied(warehousePlaceA2, -(productStock1A2.getQuantity() / 2)),
                                productStock1A2.getQuantity() / 2)
                ),
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA2.getId(),
                        productStock1A2.getQuantity(),
                        productStock(product1, withChangedOccupied(warehousePlaceA2, -productStock1A2.getQuantity()), 0)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("failedTakeProductFromStockArguments")
    @DisplayName("Take product from stock")
    void testFailedTakeProductFromStock(
            Object productId,
            Object warehousePlaceId,
            Object quantity,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<?> response = productStockClient.takeProductFromStock(warehousePlaceId, productId, quantity);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedTakeProductFromStockArguments() {
        return Stream.of(
                Arguments.of(
                        product1.getId(),
                        999,
                        productStock1A2.getQuantity(),
                        error(HttpStatus.NOT_FOUND, "Warehouse place with id [999] not found")
                ),
                Arguments.of(
                        999,
                        warehousePlaceA2.getId(),
                        productStock1A2.getQuantity(),
                        error(HttpStatus.NOT_FOUND, "Product with id [999] not found")
                ),
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA2.getId(),
                        999,
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not [%d] pcs of product with id [%d]. It only has [%d] pcs of product.",
                                warehousePlaceA2.getId(),
                                999,
                                product1.getId(),
                                productStock1A2.getQuantity()))
                ),
                Arguments.of(
                        product1.getId(),
                        warehousePlaceA2.getId(),
                        warehousePlaceA2.getCapacity(),
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not [%d] pcs of product with id [%d]. It only has [%d] pcs of product.",
                                warehousePlaceA2.getId(),
                                warehousePlaceA2.getCapacity(),
                                product1.getId(),
                                productStock1A2.getQuantity()))
                )
        );
    }

    @ParameterizedTest
    @MethodSource("successfulMoveProductArguments")
    @DisplayName("Move product - success")
    void testSuccessfulMoveProduct(
            ProductStockMoveRequestDto moveRequest,
            ProductStockMoveResponseDto expectedResult
    ) {
        ResponseDto<?> response = productStockClient.moveProduct(moveRequest);
        assertNotNull(response);
        assertEquals(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    private Stream<Arguments> successfulMoveProductArguments() {
        return Stream.of(
                Arguments.of(
                        moveRequest(
                                product1.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                productStock1A2.getQuantity() / 2),
                        moveResponse(
                                productStock(
                                        product1,
                                        withChangedOccupied(warehousePlaceA2, -(productStock1A2.getQuantity() / 2)),
                                        productStock1A2.getQuantity() / 2),
                                productStock(
                                        product1,
                                        withChangedOccupied(warehousePlaceA1, productStock1A2.getQuantity() / 2),
                                        productStock1A2.getQuantity() / 2))
                ),
                Arguments.of(
                        moveRequest(
                                product1.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                productStock1A2.getQuantity()),
                        moveResponse(
                                productStock(
                                        product1,
                                        withChangedOccupied(warehousePlaceA2, -productStock1A2.getQuantity()),
                                        0),
                                productStock(
                                        product1,
                                        withChangedOccupied(warehousePlaceA1, productStock1A2.getQuantity()),
                                        productStock1A2.getQuantity()))
                ),
                Arguments.of(
                        moveRequest(
                                product2.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                productStock2A2.getQuantity() / 2),
                        moveResponse(
                                productStock(
                                        product2,
                                        withChangedOccupied(warehousePlaceA2, -(productStock2A2.getQuantity() / 2)),
                                        productStock2A2.getQuantity() / 2),
                                productStock(
                                        product2,
                                        withChangedOccupied(warehousePlaceA1, productStock2A2.getQuantity() / 2),
                                        productStock2A1.getQuantity() + productStock2A2.getQuantity() / 2))
                )
        );
    }

    @ParameterizedTest
    @MethodSource("failedMoveProductArguments")
    @DisplayName("Move product - fail")
    void testFailedMoveProduct(
            ProductStockMoveRequestDto moveRequest,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<?> response = productStockClient.moveProduct(moveRequest);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedMoveProductArguments() {
        return Stream.of(
                Arguments.of(
                        moveRequest(
                                product1.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                999),
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not [%d] pcs of product with id [%d]. It only has [%d] pcs of product.",
                                warehousePlaceA2.getId(),
                                999,
                                product1.getId(),
                                productStock1A2.getQuantity()))
                ),
                Arguments.of(
                        moveRequest(
                                product2.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                productStock2A2.getQuantity()),
                        error(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Warehouse place with id [%d] " +
                                        "has not enough space to store [%d] pcs of product with id [%d]. " +
                                        "It may store not more than [%d] pcs of product.",
                                warehousePlaceA1.getId(),
                                productStock2A2.getQuantity(),
                                product2.getId(),
                                free(warehousePlaceA1)))
                ),
                Arguments.of(
                        moveRequest(
                                product1.getId(),
                                warehousePlaceA2.getId(),
                                warehousePlaceA1.getId(),
                                0),
                        error(HttpStatus.BAD_REQUEST, "[Only positive number of product can be moved]")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("successfulFindProductsArguments")
    @DisplayName("Find products - success")
    void testSuccessfulFindProducts(
            Object name,
            Object city,
            Object minQuantitySupplier,
            Object maxQuantitySupplier,
            Object pageNumberSupplier,
            Object pageSizeSupplier,
            ProductStockFindResponseDto expectedResult
    ) {
        ResponseDto<ProductStockFindResponseDto> response = productStockClient.findProducts(
                name,
                city,
                minQuantitySupplier,
                maxQuantitySupplier,
                pageNumberSupplier,
                pageSizeSupplier
        );
        assertNotNull(response);
        assertFindResponsesEquals(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    private Stream<Arguments> successfulFindProductsArguments() {
        return Stream.of(
                Arguments.of(
                        null,
                        null,
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock2A1,
                                productStock2A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        product1.getName(),
                        null,
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        product1.getName().substring(0, product1.getName().length() - 1),
                        null,
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock2A1,
                                productStock2A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        product1.getName().substring(3),
                        null,
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        product1.getName().substring(3, product1.getName().length() - 2),
                        null,
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock2A1,
                                productStock2A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        null,
                        warehouseA.getCity(),
                        null,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock2A1,
                                productStock2A2
                        )
                ),
                Arguments.of(
                        null,
                        null,
                        50,
                        null,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock2A1,
                                productStock2A2
                        )
                ),
                Arguments.of(
                        null,
                        null,
                        null,
                        50,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2,
                                productStock1B1
                        )
                ),
                Arguments.of(
                        product1.getName().substring(0, product1.getName().length() - 1),
                        warehouseA.getCity(),
                        10,
                        50,
                        0,
                        defaultPageSize,
                        singlePageFindResponse(
                                productStock1A2
                        )
                ),
                Arguments.of(
                        product2.getName(),
                        warehouseB.getCity(),
                        10,
                        50,
                        0,
                        defaultPageSize,
                        emptyFindResponse(),
                        null
                ),
                Arguments.of(
                        null,
                        null,
                        null,
                        null,
                        0,
                        2,
                        findResponse(
                                0,
                                2,
                                2,
                                4,
                                productStock1A2,
                                productStock2A1
                        )
                ),
                Arguments.of(
                        null,
                        null,
                        null,
                        null,
                        1,
                        2,
                        findResponse(
                                1,
                                2,
                                2,
                                4,
                                productStock2A2,
                                productStock1B1
                        )
                )
        );
    }

    @Test
    @DisplayName("Find products - fail")
    void testFailedFindProducts() {
        ResponseDto<ProductStockFindResponseDto> response = productStockClient.findProducts(
                null,
                null,
                "NaN",
                "NaN",
                0,
                defaultPageSize
        );
        ResponseDto.ResponseErrorDto expectedError = error(HttpStatus.BAD_REQUEST, "Failed to convert " +
                "value of type 'java.lang.String' to required type 'java.lang.Integer'; " +
                "nested exception is java.lang.NumberFormatException: For input string: \"NaN\"");
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private static ProductStockDto productStock(ProductResponseDto product, WarehousePlaceDto warehousePlace, int quantity) {
        return ProductStockDto.builder()
                .product(product)
                .warehousePlace(warehousePlace)
                .quantity(quantity)
                .build();
    }

    private static ProductStockMoveRequestDto moveRequest(long productId, long fromWarehousePlaceId, long toWarehousePlaceId, int quantity) {
        return ProductStockMoveRequestDto.builder()
                .productId(productId)
                .fromWarehousePlaceId(fromWarehousePlaceId)
                .toWarehousePlaceId(toWarehousePlaceId)
                .quantity(quantity)
                .build();
    }

    private static ProductStockMoveResponseDto moveResponse(ProductStockDto movedFrom, ProductStockDto movedTo) {
        return ProductStockMoveResponseDto.builder().movedFrom(movedFrom).movedTo(movedTo).build();
    }

    private static int free(WarehousePlaceDto warehousePlace) {
        return warehousePlace.getCapacity() - warehousePlace.getOccupied();
    }

    private static WarehousePlaceDto withChangedOccupied(WarehousePlaceDto warehousePlace, int occupiedChange) {
        return WarehousePlaceDto.builder()
                .id(warehousePlace.getId())
                .warehouse(warehousePlace.getWarehouse())
                .row(warehousePlace.getRow())
                .shelf(warehousePlace.getShelf())
                .capacity(warehousePlace.getCapacity())
                .occupied(warehousePlace.getOccupied() + occupiedChange)
                .build();
    }

    private static ProductStockFindResponseDto findResponse(
            int pageNumber,
            int pageSize,
            int totalPages,
            long totalElements,
            ProductStockDto... content
    ) {
        return ProductStockFindResponseDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .totalPages(totalPages)
                .totalElements(totalElements)
                .content(Arrays.asList(content))
                .build();
    }

    private static ProductStockFindResponseDto singlePageFindResponse(ProductStockDto... content) {
        return findResponse(0, defaultPageSize, 1, content.length, content);
    }

    private static ProductStockFindResponseDto emptyFindResponse() {
        return findResponse(0, defaultPageSize, 0, 0);
    }

    private static void assertFindResponsesEquals(ProductStockFindResponseDto a, ProductStockFindResponseDto b) {
        if (a == null || b == null) {
            assertEquals(a, b);
            return;
        }
        assertEquals(a.getPageNumber(), b.getPageNumber());
        assertEquals(a.getPageSize(), b.getPageSize());
        assertEquals(a.getTotalPages(), b.getTotalPages());
        assertEquals(a.getTotalElements(), b.getTotalElements());
        TestUtils.assertContainsEqualElements(a.getContent(), b.getContent());
    }

}
