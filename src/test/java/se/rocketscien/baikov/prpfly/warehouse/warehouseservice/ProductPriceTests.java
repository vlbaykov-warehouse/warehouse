package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client.PriceServiceClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.ProductPriceNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.service.ExternalProductPriceService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ProductPriceTests extends WarehouseApplicationTests {

    @Autowired
    PriceServiceClient priceServiceClient;
    @Autowired
    ExternalProductPriceService productPriceService;

    @Test
    void testFetchPrices() {
        assertEquals(product4.getPrice(), productPriceService.getProductPrice(product4.getId()));
        assertEquals(product5.getPrice(), productPriceService.getProductPrice(product5.getId()));
        TestUtils.assertThrows(
                ProductPriceNotFoundException.class,
                "product-price.not-found",
                () -> productPriceService.getProductPrice(product6.getId()));
        TestUtils.assertThrows(
                ProductPriceNotFoundException.class,
                "product-price.not-found",
                () -> productPriceService.getProductPrice(product7.getId()));

        PriceDto product5NewPrice = priceDto(product5.getId(), new BigDecimal(2).setScale(4, RoundingMode.HALF_UP));
        PriceDto product6NewPrice = priceDto(product6.getId(), new BigDecimal(4).setScale(4, RoundingMode.HALF_UP));
        when(priceServiceClient.getPricesByProductIds(eq(Set.of(2L, 3L, 6L, 7L)))).thenReturn(List.of(product6NewPrice));
        when(priceServiceClient.getPricesByProductIds(eq(Set.of(5L)))).thenReturn(List.of(product5NewPrice));

        productPriceService.fetchPrices();

        assertEquals(product4.getPrice(), productPriceService.getProductPrice(product4.getId()));
        assertEquals(product5NewPrice.getPrice(), productPriceService.getProductPrice(product5.getId()));
        assertEquals(product6NewPrice.getPrice(), productPriceService.getProductPrice(product6.getId()));
        TestUtils.assertThrows(
                ProductPriceNotFoundException.class,
                "product-price.not-found",
                () -> productPriceService.getProductPrice(product7.getId()));
    }

    private static PriceDto priceDto(long productId, BigDecimal price) {
        return PriceDto.builder().productId(productId).price(price).build();
    }

}
