package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client.ProductClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client.ProductStockClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client.PriceServiceClient;

import java.util.Map;

@TestConfiguration
@EnableKafka
public class WarehouseApplicationTestsConfiguration {

    @MockBean
    PriceServiceClient priceServiceClient;

    @Bean
    public KafkaContainer kafka() {
        KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"));
        kafka.start();
        return kafka;
    }

    @Bean
    public ProducerFactory<?, ?> producerFactory(KafkaContainer kafka) {
        return new DefaultKafkaProducerFactory<>(
                Map.of(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers()),
                new LongSerializer(),
                new JsonSerializer<>()
        );
    }

    @Bean
    public ConsumerFactory<Long, RatingDto> consumerFactory(KafkaContainer kafka) {
        JsonDeserializer<RatingDto> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.trustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                Map.of(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers(),
                        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"),
                new LongDeserializer(),
                jsonDeserializer
        );
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Long, RatingDto> kafkaListenerContainerFactory(
            ConsumerFactory<Long, RatingDto> consumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<Long, RatingDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    @Lazy
    public ProductClient productClient(TestRestTemplate testRestTemplate, @Value("${local.server.port}") int port) {
        return new ProductClient(testRestTemplate, port);
    }

    @Bean
    @Lazy
    public ProductStockClient productStockClient(TestRestTemplate testRestTemplate, @Value("${local.server.port}") int port) {
        return new ProductStockClient(testRestTemplate, port);
    }

}
