package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client.ProductClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.rating.RatingServiceClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.service.ExternalProductRatingService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@WireMockTest(httpPort = 8082)
class ProductRatingTests extends WarehouseApplicationTests {

    static String ratingEndpoint = "/api/v1/rating/product/{productId}";
    static String emptyResponse = TestUtils.getJsonResource("empty_response.json");

    @Autowired
    RatingServiceClient ratingServiceClient;
    @Autowired
    KafkaTemplate<Long, RatingDto> kafkaTemplate;
    @Autowired
    WarehouseProperties properties;
    @Autowired
    ExternalProductRatingService productRatingService;
    @Autowired
    ProductClient productClient;

    @Test
    void testRequestRatings() {
        ProductResponseDto product = createProduct("product-with-rating");
        RatingDto rating = ratingDto(product.getId(), new BigDecimal(6).setScale(2, RoundingMode.HALF_UP));
        ProductResponseDto expectedProductWithRating = productWithRating(product, rating);

        String url = ratingEndpoint.replace("{productId}", String.valueOf(rating.getProductId()));
        stubFor(post(urlEqualTo(url)).willReturn(okJson(emptyResponse)));

        productRatingService.requestRatings();
        verify(postRequestedFor(urlEqualTo(url)));
        sendRatingToKafka(rating);
        await().atMost(Duration.ofSeconds(5)).pollInterval(Duration.ofMillis(200)).untilAsserted(() -> {
            ResponseDto<ProductResponseDto> response = productClient.getProductById(product.getId());
            assertNotNull(response);
            assertEquals(expectedProductWithRating, response.getResult());
            assertNull(response.getError());
        });
    }

    private ProductResponseDto createProduct(@SuppressWarnings("SameParameterValue") String name) {
        ResponseDto<ProductResponseDto> response = productClient.createProduct(productRequest(name));
        assertNotNull(response);
        assertNotNull(response.getResult());
        assertNull(response.getError());
        return response.getResult();
    }

    private void sendRatingToKafka(RatingDto ratingDto) {
        kafkaTemplate.send(
                properties.getIntegration().getRating().getKafka().getTopic().getName(),
                ratingDto.getProductId(),
                ratingDto
        );
    }

    private static RatingDto ratingDto(long productId, BigDecimal rating) {
        return RatingDto.builder().productId(productId).rating(rating).build();
    }

    private static ProductResponseDto productWithRating(ProductResponseDto product, RatingDto rating) {
        return ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .rating(rating.getRating())
                .build();
    }

}
