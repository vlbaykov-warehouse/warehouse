package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client.ProductClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductTests extends WarehouseApplicationTests {

    @Autowired
    ProductClient productClient;

    @Test
    @DisplayName("Get product by id - success")
    void testSuccessfulGetProductById() {
        ResponseDto<ProductResponseDto> response = productClient.getProductById(product1.getId());
        assertNotNull(response);
        assertEquals(product1, response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("failedGetProductByIdArguments")
    @DisplayName("Get product by id - fail")
    void testFailedGetProductById(Object productId, ResponseDto.ResponseErrorDto expectedError) {
        ResponseDto<ProductResponseDto> response = productClient.getProductById(productId);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedGetProductByIdArguments() {
        return Stream.of(
                Arguments.of(
                        999,
                        error(HttpStatus.NOT_FOUND, "Product with id [999] not found")
                ),
                Arguments.of(
                        "text",
                        error(HttpStatus.BAD_REQUEST, "Failed to convert " +
                                "value of type 'java.lang.String' to required type 'long'; nested exception is " +
                                "java.lang.NumberFormatException: For input string: \"text\"")
                )
        );
    }

    @Test
    @DisplayName("Create product - success")
    void testSuccessfulCreateProduct() {
        ProductRequestDto request = productRequest("CreatedProduct");
        ProductResponseDto expectedResult = productResponse(null, request.getName());
        ResponseDto<ProductResponseDto> response = productClient.createProduct(request);
        assertNotNull(response);
        assertEqualsWithoutId(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("failedCreateProductArguments")
    @DisplayName("Create product - fail")
    void testFailedCreateProduct(ProductRequestDto request, ResponseDto.ResponseErrorDto expectedError) {
        ResponseDto<ProductResponseDto> response = productClient.createProduct(request);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedCreateProductArguments() {
        return Stream.of(
                Arguments.of(
                        productRequest(null),
                        error(HttpStatus.BAD_REQUEST, "[Product name shouldn't be blank]")
                ),
                Arguments.of(
                        productRequest("n"),
                        error(HttpStatus.BAD_REQUEST, "[Product name length should be in range [3; 255]]")
                ),
                Arguments.of(
                        productRequest("t".repeat(256)),
                        error(HttpStatus.BAD_REQUEST, "[Product name length should be in range [3; 255]]")
                ),
                Arguments.of(
                        productRequest("product~!"),
                        error(HttpStatus.BAD_REQUEST, "[Product name should only contain digits, latin letters, spaces, '_', '-']")
                )
        );
    }

    @Test
    @DisplayName("Update product - success")
    void testSuccessfulUpdateProduct() {
        ProductRequestDto request = productRequest("UpdatedProduct");
        ProductResponseDto expectedResult = productResponse(product2.getId(), request.getName());
        ResponseDto<ProductResponseDto> response = productClient.updateProduct(product2.getId(), request);
        assertNotNull(response);
        assertEquals(expectedResult, response.getResult());
        assertNull(response.getError());
    }

    @ParameterizedTest
    @MethodSource("failedUpdateProductArguments")
    @DisplayName("Update product - fail")
    void testFailedUpdateProduct(
            Object productId,
            ProductRequestDto request,
            ResponseDto.ResponseErrorDto expectedError
    ) {
        ResponseDto<ProductResponseDto> response = productClient.updateProduct(productId, request);
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

    private Stream<Arguments> failedUpdateProductArguments() {
        return Stream.of(
                Arguments.of(
                        product2.getId(),
                        productRequest(null),
                        error(HttpStatus.BAD_REQUEST, "[Product name shouldn't be blank]")
                ),
                Arguments.of(
                        product2.getId(),
                        productRequest("T"),
                        error(HttpStatus.BAD_REQUEST, "[Product name length should be in range [3; 255]]")
                ),
                Arguments.of(
                        product2.getId(),
                        productRequest("a".repeat(256)),
                        error(HttpStatus.BAD_REQUEST, "[Product name length should be in range [3; 255]]")
                ),
                Arguments.of(
                        product2.getId(),
                        productRequest("TestPr/??"),
                        error(HttpStatus.BAD_REQUEST, "[Product name should only contain digits, latin letters, spaces, '_', '-']")
                ),
                Arguments.of(
                        999,
                        productRequest("TestProduct2"),
                        error(HttpStatus.NOT_FOUND, "Product with id [999] not found")
                )
        );
    }

    @Test
    @DisplayName("Delete product - success")
    void testSuccessfulDeleteProduct() {
        ResponseDto<?> response = productClient.deleteProduct(product3.getId());
        assertNotNull(response);
        assertNull(response.getResult());
        assertNull(response.getError());
    }

    @Test
    @DisplayName("Delete product - fail")
    void testFailedDeleteProduct() {
        ResponseDto<?> response = productClient.deleteProduct(999);
        ResponseDto.ResponseErrorDto expectedError = error(HttpStatus.NOT_FOUND, "Product with id [999] not found");
        assertNotNull(response);
        assertNull(response.getResult());
        assertEquals(expectedError, response.getError());
    }

}
