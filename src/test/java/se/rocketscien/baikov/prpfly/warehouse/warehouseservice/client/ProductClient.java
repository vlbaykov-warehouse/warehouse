package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;

public class ProductClient {

    private static final String basicUrl = "http://localhost:{port}/api/v1/product/";
    private static final String productUrl = basicUrl + "{productId}";
    private static final ParameterizedTypeReference<ResponseDto<ProductResponseDto>> PRODUCT_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private static final ParameterizedTypeReference<ResponseDto<?>> EMPTY_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private final TestRestTemplate restTemplate;
    private final int port;

    public ProductClient(TestRestTemplate restTemplate, int port) {
        this.restTemplate = restTemplate;
        this.port = port;
    }

    public ResponseDto<ProductResponseDto> getProductById(Object productId) {
        return restTemplate
                .exchange(productUrl, HttpMethod.GET, HttpEntity.EMPTY, PRODUCT_RESPONSE_TR, port, productId)
                .getBody();
    }

    public ResponseDto<ProductResponseDto> createProduct(ProductRequestDto request) {
        return restTemplate
                .exchange(basicUrl, HttpMethod.POST, new HttpEntity<>(request), PRODUCT_RESPONSE_TR, port)
                .getBody();
    }

    public ResponseDto<ProductResponseDto> updateProduct(Object productId, ProductRequestDto request) {
        return restTemplate
                .exchange(productUrl, HttpMethod.PUT, new HttpEntity<>(request), PRODUCT_RESPONSE_TR, port, productId)
                .getBody();
    }

    public ResponseDto<?> deleteProduct(Object productId) {
        return restTemplate
                .exchange(productUrl, HttpMethod.DELETE, HttpEntity.EMPTY, EMPTY_RESPONSE_TR, port, productId)
                .getBody();
    }

}
