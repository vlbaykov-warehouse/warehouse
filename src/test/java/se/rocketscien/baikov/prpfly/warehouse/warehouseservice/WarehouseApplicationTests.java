package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.junit.jupiter.Testcontainers;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse.WarehouseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceDto;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(WarehouseApplicationTestsConfiguration.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers
@Sql(scripts = "/db/sql/before_each_test.sql")
abstract class WarehouseApplicationTests {

    static ProductResponseDto product1 = ProductResponseDto.builder()
            .id(1L)
            .name("TestProduct1")
            .price(BigDecimal.TEN.setScale(4, RoundingMode.HALF_UP))
            .rating(BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP))
            .build();
    static ProductResponseDto product2 = ProductResponseDto.builder()
            .id(2L)
            .name("TestProduct2")
            .build();
    static ProductResponseDto product3 = ProductResponseDto.builder()
            .id(3L)
            .name("TestProduct3")
            .build();
    static ProductResponseDto product4 = ProductResponseDto.builder()
            .id(4L)
            .name("TestProduct4")
            .price(BigDecimal.TEN.setScale(4, RoundingMode.HALF_UP))
            .rating(BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP))
            .build();
    static ProductResponseDto product5 = ProductResponseDto.builder()
            .id(5L)
            .name("TestProduct5")
            .price(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP))
            .rating(BigDecimal.ONE.setScale(2, RoundingMode.HALF_UP))
            .build();
    static ProductResponseDto product6 = ProductResponseDto.builder()
            .id(6L)
            .name("TestProduct6")
            .build();
    static ProductResponseDto product7 = ProductResponseDto.builder()
            .id(7L)
            .name("TestProduct7")
            .build();
    static WarehouseDto warehouseA = WarehouseDto.builder()
            .id(1L)
            .name("TestWarehouse1")
            .city("Test City")
            .build();
    static WarehouseDto warehouseB = WarehouseDto.builder()
            .id(2L)
            .name("TestWarehouse2")
            .city("City Test")
            .build();
    static WarehousePlaceDto warehousePlaceA1 = WarehousePlaceDto.builder()
            .id(1L)
            .warehouse(warehouseA)
            .row(1)
            .shelf(1)
            .capacity(100)
            .occupied(60)
            .build();
    static WarehousePlaceDto warehousePlaceA2 = WarehousePlaceDto.builder()
            .id(2L)
            .warehouse(warehouseA)
            .row(1)
            .shelf(2)
            .capacity(100)
            .occupied(100)
            .build();
    static WarehousePlaceDto warehousePlaceB1 = WarehousePlaceDto.builder()
            .id(3L)
            .warehouse(warehouseB)
            .row(1)
            .shelf(1)
            .capacity(100)
            .occupied(20)
            .build();
    static ProductStockDto productStock1A2 = ProductStockDto.builder()
            .product(product1)
            .warehousePlace(warehousePlaceA2)
            .quantity(40)
            .build();
    static ProductStockDto productStock2A1 = ProductStockDto.builder()
            .product(product2)
            .warehousePlace(warehousePlaceA1)
            .quantity(60)
            .build();
    static ProductStockDto productStock2A2 = ProductStockDto.builder()
            .product(product2)
            .warehousePlace(warehousePlaceA2)
            .quantity(60)
            .build();
    static ProductStockDto productStock1B1 = ProductStockDto.builder()
            .product(product1)
            .warehousePlace(warehousePlaceB1)
            .quantity(20)
            .build();

    static ResponseDto.ResponseErrorDto error(HttpStatus status, String message) {
        return new ResponseDto.ResponseErrorDto(status.value(), message);
    }

    static ProductRequestDto productRequest(String name) {
        return ProductRequestDto.builder().name(name).build();
    }

    static ProductResponseDto productResponse(Long id, String name) {
        return ProductResponseDto.builder().id(id).name(name).build();
    }

    static void assertEqualsWithoutId(ProductResponseDto expected, ProductResponseDto actual) {
        if (expected == null || actual == null) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getPrice(), actual.getPrice());
    }

}
