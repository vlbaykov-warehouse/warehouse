package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.client;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockFindResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveResponseDto;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ProductStockClient {

    private static final String basicUrl = "http://localhost:{port}/api/v1/stock";
    private static final String putUrl = basicUrl + "/place/{warehousePlaceId}/product/{productId}/put?quantity={quantity}";
    private static final String takeUrl = basicUrl + "/place/{warehousePlaceId}/product/{productId}/take?quantity={quantity}";
    private static final String moveUrl = basicUrl + "/move";
    private static final int defaultPageSize = 10;
    private static final ParameterizedTypeReference<ResponseDto<ProductStockDto>> PRODUCT_STOCK_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private static final ParameterizedTypeReference<ResponseDto<ProductStockMoveResponseDto>> MOVE_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private static final ParameterizedTypeReference<ResponseDto<ProductStockFindResponseDto>> FIND_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private final TestRestTemplate restTemplate;
    private final int port;

    public ResponseDto<ProductStockDto> putProductInStock(Object warehousePlaceId, Object productId, Object quantity) {
        return restTemplate
                .exchange(putUrl, HttpMethod.POST, HttpEntity.EMPTY, PRODUCT_STOCK_RESPONSE_TR, port, warehousePlaceId, productId, quantity)
                .getBody();
    }

    public ResponseDto<ProductStockDto> takeProductFromStock(Object warehousePlaceId, Object productId, Object quantity) {
        return restTemplate
                .exchange(takeUrl, HttpMethod.POST, HttpEntity.EMPTY, PRODUCT_STOCK_RESPONSE_TR, port, warehousePlaceId, productId, quantity)
                .getBody();
    }

    public ResponseDto<ProductStockMoveResponseDto> moveProduct(ProductStockMoveRequestDto moveRequest) {
        return restTemplate
                .exchange(moveUrl, HttpMethod.POST, new HttpEntity<>(moveRequest), MOVE_RESPONSE_TR, port)
                .getBody();
    }

    public ResponseDto<ProductStockFindResponseDto> findProducts(
            Object name,
            Object city,
            Object minQuantity,
            Object maxQuantity,
            Object pageNumber,
            Object pageSize
    ) {
        String url = basicUrl + "?";
        List<Object> args = new ArrayList<>();
        args.add(port);
        if (name != null) {
            url += "name={name}&";
            args.add(name);
        }
        if (city != null) {
            url += "city={city}&";
            args.add(city);
        }
        if (minQuantity != null) {
            url += "minQuantity={minQuantity}&";
            args.add(minQuantity);
        }
        if (maxQuantity != null) {
            url += "maxQuantity={maxQuantity}&";
            args.add(maxQuantity);
        }
        if (pageNumber != null) {
            url += "page={page}&";
            args.add(pageNumber);
        }
        if (pageSize != null) {
            url += "size={size}&";
            args.add(pageSize);
        } else {
            url += String.format("size=%d&", defaultPageSize);
        }
        return restTemplate
                .exchange(url, HttpMethod.GET, HttpEntity.EMPTY, FIND_RESPONSE_TR, args.toArray())
                .getBody();
    }

}
