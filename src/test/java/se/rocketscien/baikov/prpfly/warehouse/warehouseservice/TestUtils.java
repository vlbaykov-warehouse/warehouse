package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import com.github.jknack.handlebars.internal.Files;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

public class TestUtils {

    public static <T> void assertContainsEqualElements(Collection<T> expected, Collection<T> actual) {
        if (expected == null || actual == null) {
            Assertions.assertEquals(expected, actual);
            return;
        }
        Assertions.assertEquals(expected.size(), actual.size());
        Assertions.assertTrue(expected.containsAll(actual));
        Assertions.assertTrue(actual.containsAll(expected));
    }

    public static void assertThrows(Class<? extends Exception> exceptionType, String expectedMessage, Executable executable) {
        Exception exception = Assertions.assertThrows(exceptionType, executable);
        Assertions.assertEquals(expectedMessage, exception.getMessage());
    }

    public static String getJsonResource(String jsonName) {
        return getResource("/json/" + jsonName);
    }

    public static String getResource(String resourceName) {
        try (InputStream inputStream = TestUtils.class.getResourceAsStream(resourceName)) {
            assert inputStream != null;
            return Files.read(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
