package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client.PriceServiceClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client.PriceServiceClientImpl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@WireMockTest(httpPort = 8081)
class PriceServiceClientTests {

    static String priceServiceUrl = "http://localhost:8081";
    static String priceEndpoint = "/api/v1/price/product/{productId}";
    static String pricesEndpoint = "/api/v1/price/product";
    static TypeReference<ResponseDto<PriceDto>> PRICE_RESPONSE_TR = new TypeReference<>() {
    };
    static TypeReference<ResponseDto<List<PriceDto>>> PRICES_RESPONSE_TR = new TypeReference<>() {
    };
    static String priceOkResponse = TestUtils.getJsonResource("price_ok_response.json");
    static String priceNotFoundResponse = TestUtils.getJsonResource("price_not_found_response.json");
    static String emptyResponse = TestUtils.getJsonResource("empty_response.json");
    static String pricesOkResponse = TestUtils.getJsonResource("prices_ok_response.json");
    static String pricesErrorResponse = TestUtils.getJsonResource("prices_error_response.json");
    static ObjectMapper objectMapper = new ObjectMapper();

    PriceServiceClient priceServiceClient;

    PriceServiceClientTests() {
        WarehouseProperties properties = new WarehouseProperties();
        properties.getIntegration().getPrice().setUrl(priceServiceUrl);
        properties.getIntegration().getPrice().setPriceGetEndpoint(priceEndpoint);
        properties.getIntegration().getPrice().setPricesGetEndpoint(pricesEndpoint);
        priceServiceClient = new PriceServiceClientImpl(new RestTemplate(), properties);
    }

    @Test
    @DisplayName("Get price by product id - success")
    void testSuccessfulGetPriceByProductId() throws Exception {
        PriceDto expected = objectMapper.readValue(priceOkResponse, PRICE_RESPONSE_TR).getResult();
        String url = priceEndpoint.replace("{productId}", String.valueOf(expected.getProductId()));
        stubFor(get(urlEqualTo(url)).willReturn(okJson(priceOkResponse)));

        PriceDto actual = priceServiceClient.getPriceByProductId(expected.getProductId());
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("failedGetPriceByProductIdArguments")
    @DisplayName("Get price by product id - fail")
    void testFailedGetPriceByProductId(
            String url,
            long productId,
            ResponseDefinitionBuilder response,
            Class<Exception> expectedExceptionType,
            String expectedMessage
    ) {
        stubFor(get(urlEqualTo(url)).willReturn(response));
        Exception exception = assertThrows(expectedExceptionType, () -> priceServiceClient.getPriceByProductId(productId));
        assertEquals(expectedMessage, exception.getMessage());
    }

    static Stream<Arguments> failedGetPriceByProductIdArguments() {
        return Stream.of(
                Arguments.of(
                        priceEndpoint.replace("{productId}", String.valueOf(2)),
                        2,
                        notFound().withBody(priceNotFoundResponse.replace("{0}", String.valueOf(2))),
                        RestClientException.class,
                        "404 Not Found: \"" + priceNotFoundResponse.replace("{0}", String.valueOf(2)).replaceAll("\n", "<EOL>") + "\""
                ),
                Arguments.of(
                        priceEndpoint.replace("{productId}", String.valueOf(1)),
                        1,
                        ok(),
                        IllegalStateException.class,
                        "Body of successful response cannot be null"
                ),
                Arguments.of(
                        priceEndpoint.replace("{productId}", String.valueOf(1)),
                        1,
                        okJson(priceNotFoundResponse.replace("{0}", String.valueOf(1))),
                        IllegalStateException.class,
                        "Successful response cannot contain error: [ResponseDto.ResponseErrorDto(code=404, message=Price for product with id [1] not found)]"
                ),
                Arguments.of(
                        priceEndpoint.replace("{productId}", String.valueOf(1)),
                        1,
                        okJson(emptyResponse),
                        IllegalStateException.class,
                        "Result of successful response cannot be null"
                ),
                Arguments.of(
                        priceEndpoint.replace("{productId}", String.valueOf(1)),
                        1,
                        okJson(pricesOkResponse),
                        RestClientException.class,
                        "Error while extracting response for type " +
                                "[se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto<se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto>] " +
                                "and content type [application/json]; nested exception is org.springframework.http.converter.HttpMessageNotReadableException: " +
                                "JSON parse error: Cannot deserialize value of type " +
                                "`se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto$PriceDtoBuilder` " +
                                "from Array value (token `JsonToken.START_ARRAY`); nested exception is " +
                                "com.fasterxml.jackson.databind.exc.MismatchedInputException: Cannot deserialize value of type " +
                                "`se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto$PriceDtoBuilder` " +
                                "from Array value (token `JsonToken.START_ARRAY`)\n" +
                                " at [Source: (org.springframework.util.StreamUtils$NonClosingInputStream); line: 2, column: 13] " +
                                "(through reference chain: se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto[\"result\"])"
                )
        );
    }

    @Test
    @DisplayName("Get prices by product ids - success")
    void testSuccessfulGetPricesByProductIds() throws Exception {
        List<PriceDto> expected = objectMapper.readValue(pricesOkResponse, PRICES_RESPONSE_TR).getResult();
        Set<Long> productIds = expected.stream().map(PriceDto::getProductId).collect(Collectors.toSet());
        stubFor(post(urlEqualTo(pricesEndpoint))
                .withRequestBody(equalTo(objectMapper.writeValueAsString(productIds)))
                .willReturn(okJson(pricesOkResponse)));

        List<PriceDto> actual = priceServiceClient.getPricesByProductIds(productIds);
        TestUtils.assertContainsEqualElements(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("failedGetPricesByProductIdsArguments")
    @DisplayName("Get prices by product ids - fail")
    void testFailedGetPricesByProductIds(
            String url,
            Set<Long> productIds,
            ResponseDefinitionBuilder response,
            Class<Exception> expectedExceptionType,
            String expectedMessage
    ) throws Exception {
        stubFor(post(urlEqualTo(url))
                .withRequestBody(equalTo(objectMapper.writeValueAsString(productIds)))
                .willReturn(response));
        Exception exception = assertThrows(expectedExceptionType, () -> priceServiceClient.getPricesByProductIds(productIds));
        assertEquals(expectedMessage, exception.getMessage());
    }

    static Stream<Arguments> failedGetPricesByProductIdsArguments() {
        return Stream.of(
                Arguments.of(
                        pricesEndpoint,
                        Set.of(1, 2),
                        serverError().withBody(pricesErrorResponse),
                        RestClientException.class,
                        "500 Server Error: \"" + pricesErrorResponse.replaceAll("\n", "<EOL>") + "\""
                ),
                Arguments.of(
                        pricesEndpoint,
                        Set.of(1, 2),
                        ok(),
                        IllegalStateException.class,
                        "Body of successful response cannot be null"
                ),
                Arguments.of(
                        pricesEndpoint,
                        Set.of(1, 2),
                        okJson(pricesErrorResponse),
                        IllegalStateException.class,
                        "Successful response cannot contain error: [ResponseDto.ResponseErrorDto(code=500, message=Unknown error)]"
                ),
                Arguments.of(
                        pricesEndpoint,
                        Set.of(1, 2),
                        okJson(emptyResponse),
                        IllegalStateException.class,
                        "Result of successful response cannot be null"
                ),
                Arguments.of(
                        pricesEndpoint,
                        Set.of(1, 2),
                        okJson(priceOkResponse),
                        RestClientException.class,
                        "Error while extracting response for type " +
                                "[se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto<java.util.List<se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto>>] " +
                                "and content type [application/json]; nested exception is org.springframework.http.converter.HttpMessageNotReadableException: " +
                                "JSON parse error: Cannot deserialize value of type `java.util.ArrayList<se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto>` " +
                                "from Object value (token `JsonToken.START_OBJECT`); nested exception is com.fasterxml.jackson.databind.exc.MismatchedInputException: " +
                                "Cannot deserialize value of type `java.util.ArrayList<se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto>` " +
                                "from Object value (token `JsonToken.START_OBJECT`)\n" +
                                " at [Source: (org.springframework.util.StreamUtils$NonClosingInputStream); line: 2, column: 13] " +
                                "(through reference chain: se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto[\"result\"])"
                )
        );
    }

}
