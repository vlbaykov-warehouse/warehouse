TRUNCATE products RESTART IDENTITY CASCADE;
TRUNCATE warehouses RESTART IDENTITY CASCADE;

INSERT INTO products(version, name)
VALUES (0, 'TestProduct1'),
       (0, 'TestProduct2'),
       (0, 'TestProduct3'),
       (0, 'TestProduct4'),
       (0, 'TestProduct5'),
       (0, 'TestProduct6'),
       (0, 'TestProduct7');

INSERT INTO warehouses(version, name, city)
VALUES (0, 'TestWarehouse1', 'Test City'),
       (0, 'TestWarehouse2', 'City Test');

INSERT INTO warehouse_places(version, warehouse_id, row, shelf, capacity, occupied)
VALUES (0, 1, 1, 1, 100, 60),
       (0, 1, 1, 2, 100, 100),
       (0, 2, 1, 1, 100, 20);

INSERT INTO product_stocks(version, product_id, warehouse_place_id, quantity)
VALUES (0, 1, 2, 40),
       (0, 2, 1, 60),
       (0, 2, 2, 60),
       (0, 1, 3, 20);

INSERT INTO product_prices(version, product_id, amount, last_update)
VALUES (0, 1, 10.0000, now()),
       (0, 4, 10.0000, now()),
       (0, 5, 1.0000, 'epoch'::timestamp);

UPDATE products
SET price_id = 1
WHERE id = 1;

UPDATE products
SET price_id = 2
WHERE id = 4;

UPDATE products
SET price_id = 3
WHERE id = 5;

INSERT INTO product_ratings(version, product_id, amount, last_update)
VALUES (0, 1, 10.00, now()),
       (0, 4, 10.00, now()),
       (0, 5, 1.00, 'epoch'::timestamp);

UPDATE products
SET rating_id = 1
WHERE id = 1;

UPDATE products
SET rating_id = 2
WHERE id = 4;

UPDATE products
SET rating_id = 3
WHERE id = 5;