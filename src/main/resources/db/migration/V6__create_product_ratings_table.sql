CREATE TABLE product_ratings
(
    id          BIGSERIAL PRIMARY KEY,
    version     BIGINT        NOT NULL,
    product_id  BIGINT        NOT NULL REFERENCES products (id),
    amount      NUMERIC(5, 2) NOT NULL,
    last_update TIMESTAMP     NOT NULL
);

CREATE UNIQUE INDEX product_ratings_product_uidx ON product_ratings (product_id);
CREATE INDEX product_ratings_last_update_idx ON product_ratings (last_update);

ALTER TABLE products
    ADD COLUMN rating_id BIGINT REFERENCES product_ratings;