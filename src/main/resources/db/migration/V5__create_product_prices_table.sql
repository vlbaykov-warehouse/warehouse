CREATE TABLE product_prices
(
    id          BIGSERIAL PRIMARY KEY,
    version     BIGINT         NOT NULL,
    product_id  BIGINT         NOT NULL REFERENCES products (id),
    amount      NUMERIC(17, 4) NOT NULL,
    last_update TIMESTAMP      NOT NULL
);

CREATE UNIQUE INDEX product_prices_product_uidx ON product_prices (product_id);
CREATE INDEX product_prices_last_update_idx ON product_prices (last_update);

ALTER TABLE products
ADD COLUMN price_id BIGINT REFERENCES product_prices;