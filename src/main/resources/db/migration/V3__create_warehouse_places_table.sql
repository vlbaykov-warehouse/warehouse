CREATE TABLE warehouse_places
(
    id           BIGSERIAL PRIMARY KEY,
    version      BIGINT  NOT NULL,
    warehouse_id BIGINT  NOT NULL REFERENCES warehouses (id),
    row          INTEGER NOT NULL,
    shelf        INTEGER NOT NULL,
    capacity     INTEGER NOT NULL,
    occupied     INTEGER NOT NULL
);

CREATE UNIQUE INDEX warehouse_places_warehouse_row_shelf_uidx ON warehouse_places (warehouse_id, row, shelf);