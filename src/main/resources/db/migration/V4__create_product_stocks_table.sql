CREATE TABLE product_stocks
(
    id                 BIGSERIAL PRIMARY KEY,
    version            BIGINT NOT NULL,
    product_id         BIGINT NOT NULL REFERENCES products (id),
    warehouse_place_id BIGINT NOT NULL REFERENCES warehouse_places (id),
    quantity           INT    NOT NULL
);

CREATE UNIQUE INDEX product_stocks_place_product_uidx ON product_stocks (warehouse_place_id, product_id);