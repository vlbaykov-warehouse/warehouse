CREATE TABLE products
(
    id      BIGSERIAL PRIMARY KEY,
    version BIGINT       NOT NULL,
    name    VARCHAR(255) NOT NULL
);