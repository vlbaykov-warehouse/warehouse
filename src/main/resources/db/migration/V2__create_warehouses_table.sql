CREATE TABLE warehouses
(
    id      BIGSERIAL PRIMARY KEY,
    version BIGINT       NOT NULL,
    name    VARCHAR(255) NOT NULL,
    city    VARCHAR(255) NOT NULL
);

CREATE UNIQUE INDEX warehouses_name_city_uidx ON warehouses (name, city);