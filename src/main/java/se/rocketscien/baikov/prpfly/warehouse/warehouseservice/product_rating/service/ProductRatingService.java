package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.service;

import java.math.BigDecimal;

public interface ProductRatingService {

    BigDecimal getProductRating(long productId);

}
