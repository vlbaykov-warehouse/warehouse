package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockMoveResult;

public interface ProductStockService {

    ProductStockEntity putProduct(long warehousePlaceId, long productId, int quantity);

    ProductStockEntity takeProduct(long warehousePlaceId, long productId, int quantity);

    ProductStockMoveResult moveProduct(long productId, long fromWarehousePlaceId, long toWarehousePlaceId, int quantity);

    Page<ProductStockEntity> findProducts(String name, String city, Integer minQuantity, Integer maxQuantity, Pageable pageable);

}
