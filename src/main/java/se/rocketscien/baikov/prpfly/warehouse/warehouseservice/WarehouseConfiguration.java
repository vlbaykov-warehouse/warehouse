package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;

import java.util.Map;

@Configuration
@EnableWebMvc
@EnableScheduling
@EnableFeignClients
@EnableKafka
public class WarehouseConfiguration {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    @ConditionalOnProperty(name = "warehouse.integration.rating.enable-requesting", havingValue = "true")
    public ConsumerFactory<Long, RatingDto> consumerFactory(
            @Value("${spring.kafka.consumer.bootstrap-servers}") String bootstrapServers
    ) {
        JsonDeserializer<RatingDto> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.trustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                Map.of(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers),
                new LongDeserializer(),
                jsonDeserializer
        );
    }

    @Bean
    @ConditionalOnProperty(name = "warehouse.integration.rating.enable-requesting", havingValue = "true")
    public ConcurrentKafkaListenerContainerFactory<Long, RatingDto> kafkaListenerContainerFactory(
            ConsumerFactory<Long, RatingDto> consumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<Long, RatingDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

}
