package se.rocketscien.baikov.prpfly.warehouse.warehouseservice;

import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@ConfigurationProperties("warehouse")
@Data
@Validated
public class WarehouseProperties {

    @Valid
    private Integration integration = new Integration();

    @Data
    public static class Integration {

        @Valid
        private PriceIntegration price = new PriceIntegration();

        @Valid
        private RatingIntegration rating = new RatingIntegration();

    }

    @Data
    public static class PriceIntegration {

        @NotBlank
        @URL
        @Pattern(regexp = "^.+\\w$")
        private String url;

        @NotBlank
        @Pattern(regexp = "^/[\\w/{}]+$")
        private String priceGetEndpoint;

        @NotBlank
        @Pattern(regexp = "^/[\\w/{}]+$")
        private String pricesGetEndpoint;

        private boolean enableFetching = true;

        @Min(1)
        private int fetchDelayMs = 5000;

        @Min(1)
        private int updateTimeoutSec = 60;

        public String getPriceGetEndpointUrl() {
            return url + priceGetEndpoint;
        }

        public String getPricesGetEndpointUrl() {
            return url + pricesGetEndpoint;
        }

    }

    @Data
    public static class RatingIntegration {

        @NotBlank
        @Pattern(regexp = "^.+\\w$")
        private String url;

        private boolean enableRequesting = true;

        @Min(1)
        private int requestDelayMs = 5000;

        @Min(1)
        private int updateTimeoutSec = 60;

        @Valid
        private KafkaRatingIntegration kafka = new KafkaRatingIntegration();

    }

    @Data
    public static class KafkaRatingIntegration {

        @NotBlank
        @Pattern(regexp = "^[\\w-.]+$")
        private String groupId;

        @Valid
        private TopicKafkaRatingIntegration topic = new TopicKafkaRatingIntegration();

    }

    @Data
    public static class TopicKafkaRatingIntegration {

        @NotBlank
        @Pattern(regexp = "^[\\w-.]+$")
        private String name;

        @Min(1)
        private int partitions;

        @Min(1)
        private int replicas;

    }

}
