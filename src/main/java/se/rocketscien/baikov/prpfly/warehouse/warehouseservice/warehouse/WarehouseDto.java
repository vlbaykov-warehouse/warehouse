package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@Schema
public class WarehouseDto {

    @Schema(title = "Warehouse ID")
    Long id;

    @Schema(title = "Name")
    String name;

    @Schema(title = "City")
    String city;

}
