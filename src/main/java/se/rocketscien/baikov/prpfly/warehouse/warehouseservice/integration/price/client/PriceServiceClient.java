package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client;

import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;

import java.util.List;
import java.util.Set;

public interface PriceServiceClient {

    PriceDto getPriceByProductId(long productId);

    List<PriceDto> getPricesByProductIds(Set<Long> productIds);

}
