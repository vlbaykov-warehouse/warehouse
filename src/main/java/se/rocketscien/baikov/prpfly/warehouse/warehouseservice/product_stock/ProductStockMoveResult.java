package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class ProductStockMoveResult {

    ProductStockEntity movedFrom;
    ProductStockEntity movedTo;

}
