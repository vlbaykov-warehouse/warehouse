package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.service;

import java.math.BigDecimal;

public interface ProductPriceService {

    BigDecimal getProductPrice(long productId);

}
