package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.CollectionUtils;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.WarehouseProperties;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client.PriceServiceClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.ProductPriceEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.ProductPriceNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.ProductPriceRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.utils.PageableUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExternalProductPriceService implements ProductPriceService {

    private final ProductPriceRepository productPriceRepository;
    private final ProductRepository productRepository;
    private final PriceServiceClient priceServiceClient;
    private final TransactionTemplate transactionTemplate;
    private final WarehouseProperties properties;
    private Pageable productsWithoutPricePageable = Pageable.ofSize(50);
    private Pageable productsWithOutdatedPricePageable = Pageable.ofSize(50);

    @Override
    public BigDecimal getProductPrice(long productId) {
        return productPriceRepository.findByProductId(productId)
                .map(ProductPriceEntity::getAmount)
                .orElseThrow(() -> new ProductPriceNotFoundException(productId));
    }

    @ConditionalOnProperty(name = "warehouse.integration.price.enable-fetching", havingValue = "true")
    @Scheduled(
            fixedDelayString = "${warehouse.integration.price.fetch-delay-ms}",
            initialDelayString = "${warehouse.integration.price.fetch-delay-ms}"
    )
    public void fetchPrices() {
        try {
            fetchNewPrices();
            fetchUpdatedPrices();
        } catch (Exception e) {
            log.error("Exception thrown while requesting product prices", e);
        }
    }

    private void fetchNewPrices() {
        Page<ProductEntity> page = productRepository.findAllByPriceIsNull(productsWithoutPricePageable);
        if (page.getNumberOfElements() < 1) {
            return;
        }
        Map<Long, ProductEntity> productMap = page.get().collect(Collectors.toMap(ProductEntity::getId, Function.identity()));
        Set<Long> productIds = productMap.keySet();
        log.info("Attempts to get price for products with ids {} for first time", productIds);
        List<PriceDto> priceDtos = priceServiceClient.getPricesByProductIds(productIds);
        List<ProductPriceEntity> prices = priceDtos.stream()
                .map(priceDto -> createPriceEntity(productMap.get(priceDto.getProductId()), priceDto))
                .toList();
        savePrices(prices);
        productsWithoutPricePageable = PageableUtils.getNextPageableWrapped(page);
    }

    private void fetchUpdatedPrices() {
        Instant since = Instant.now().minus(properties.getIntegration().getPrice().getUpdateTimeoutSec(), ChronoUnit.SECONDS);
        Page<ProductPriceEntity> page = productPriceRepository.findAllByLastUpdateLessThan(since, productsWithOutdatedPricePageable);
        if (page.getNumberOfElements() < 1) {
            return;
        }
        Map<Long, ProductPriceEntity> productIdToPriceMap = page.get()
                .collect(Collectors.toMap(price -> price.getProduct().getId(), Function.identity()));
        Set<Long> productIds = productIdToPriceMap.keySet();
        log.info("Attempts to update prices for products with ids {}", productIds);
        List<PriceDto> priceDtos = priceServiceClient.getPricesByProductIds(productIds);
        List<ProductPriceEntity> prices = priceDtos.stream()
                .map(priceDto -> updatePriceEntity(productIdToPriceMap.get(priceDto.getProductId()), priceDto))
                .toList();
        savePrices(prices);
        productsWithOutdatedPricePageable = PageableUtils.getNextPageableWrapped(page);
    }

    void savePrices(Collection<ProductPriceEntity> prices) {
        transactionTemplate.executeWithoutResult(ts -> savePrices(prices, ts));
    }

    void savePrices(Collection<ProductPriceEntity> prices, TransactionStatus ignoredTs) {
        if (CollectionUtils.isEmpty(prices)) {
            return;
        }
        Iterable<ProductPriceEntity> savedPrices = productPriceRepository.saveAll(prices);
        List<ProductEntity> productsToUpdate = new ArrayList<>(prices.size());
        for (ProductPriceEntity savedPrice : savedPrices) {
            ProductEntity product = savedPrice.getProduct();
            product.setPrice(savedPrice);
            productsToUpdate.add(product);
        }
        productRepository.saveAll(productsToUpdate);
        log.info("Saved prices {}", prices);
    }

    private static ProductPriceEntity createPriceEntity(ProductEntity product, PriceDto priceDto) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(priceDto);
        ProductPriceEntity price = new ProductPriceEntity();
        price.setProduct(product);
        return updatePriceEntity(price, priceDto);
    }

    private static ProductPriceEntity updatePriceEntity(ProductPriceEntity price, PriceDto priceDto) {
        Objects.requireNonNull(price);
        Objects.requireNonNull(priceDto);
        price.setAmount(priceDto.getPrice());
        price.setLastUpdate(Instant.now());
        return price;
    }

}
