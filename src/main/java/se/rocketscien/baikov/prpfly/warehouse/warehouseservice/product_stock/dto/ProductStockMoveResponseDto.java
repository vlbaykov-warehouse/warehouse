package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@Schema
public class ProductStockMoveResponseDto {

    @Schema(title = "Moved from stock info")
    ProductStockDto movedFrom;

    @Schema(title = "Moved to stock info")
    ProductStockDto movedTo;

}
