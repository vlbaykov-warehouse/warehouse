package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.RatingDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.WarehouseProperties;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.rating.RatingServiceClient;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.ProductRatingEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.ProductRatingNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.ProductRatingRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.utils.PageableUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExternalProductRatingService implements ProductRatingService {

    private final ProductRatingRepository productRatingRepository;
    private final ProductRepository productRepository;
    private final RatingServiceClient ratingServiceClient;
    private final TransactionTemplate transactionTemplate;
    private final WarehouseProperties properties;
    private Pageable productsWithoutRatingPageable = Pageable.ofSize(50);
    private Pageable productsWithOutdatedRatingPageable = Pageable.ofSize(50);

    @Override
    public BigDecimal getProductRating(long productId) {
        return productRatingRepository.findByProductId(productId)
                .map(ProductRatingEntity::getAmount)
                .orElseThrow(() -> new ProductRatingNotFoundException(productId));
    }

    @KafkaListener(
            topics = "${warehouse.integration.rating.kafka.topic.name}",
            groupId = "${warehouse.integration.rating.kafka.group-id}"
    )
    public void onProductRatingUpdate(@Payload RatingDto ratingDto) {
        log.info("RatingDto received: [{}]", ratingDto);
        productRepository.findById(ratingDto.getProductId()).ifPresentOrElse(
                productEntity -> saveRating(productEntity, ratingDto),
                () -> log.info("Unable to update rating of product with id [{}], product not found", ratingDto.getProductId())
        );
    }

    @ConditionalOnProperty(name = "warehouse.integration.rating.enable-requesting", havingValue = "true")
    @Scheduled(
            fixedDelayString = "${warehouse.integration.rating.request-delay-ms}",
            initialDelayString = "${warehouse.integration.rating.request-delay-ms}"
    )
    public void requestRatings() {
        try {
            requestNewRatings();
            requestUpdatedRatings();
        } catch (Exception e) {
            log.error("Exception thrown while requesting product prices", e);
        }
    }

    private void requestNewRatings() {
        Page<ProductEntity> page = productRepository.findAllByRatingIsNull(productsWithoutRatingPageable);
        if (page.getNumberOfElements() < 1) {
            return;
        }
        for (ProductEntity productEntity : page) {
            requestRating(productEntity.getId());
        }
        productsWithoutRatingPageable = PageableUtils.getNextPageableWrapped(page);
    }

    private void requestUpdatedRatings() {
        Instant since = Instant.now().minus(properties.getIntegration().getRating().getUpdateTimeoutSec(), ChronoUnit.SECONDS);
        Page<ProductRatingEntity> page = productRatingRepository.findAllByLastUpdateLessThan(since, productsWithOutdatedRatingPageable);
        for (ProductRatingEntity productRatingEntity : page) {
            requestRating(productRatingEntity.getProduct().getId());
        }
        productsWithOutdatedRatingPageable = PageableUtils.getNextPageableWrapped(page);
    }

    private void requestRating(long productId) {
        log.info("Requesting rating for product with id [{}]", productId);
        try {
            ratingServiceClient.requestRatingByProductId(productId);
        } catch (Exception e) {
            log.info("Exception thrown while requesting rating for product with id [{}]", productId, e);
        }
    }

    private void saveRating(ProductEntity productEntity, RatingDto ratingDto) {
        log.info("Saving product rating [{}]", ratingDto);
        transactionTemplate.executeWithoutResult(status -> saveRating(productEntity, ratingDto, status));
    }

    private void saveRating(ProductEntity productEntity, RatingDto ratingDto, TransactionStatus ignoredTs) {
        ProductRatingEntity productRatingEntity = productRatingRepository.findByProductId(productEntity.getId())
                .orElse(new ProductRatingEntity());
        productRatingEntity.setProduct(productEntity);
        productRatingEntity.setAmount(ratingDto.getRating());
        productRatingEntity.setLastUpdate(Instant.now());
        productRatingRepository.save(productRatingEntity);
        productEntity.setRating(productRatingEntity);
        productRepository.save(productEntity);
    }

}
