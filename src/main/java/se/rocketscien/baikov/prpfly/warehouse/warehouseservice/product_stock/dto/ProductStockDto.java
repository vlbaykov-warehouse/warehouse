package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceDto;

@Value
@Builder
@Jacksonized
@Schema
public class ProductStockDto {

    @Schema(title = "Product")
    ProductResponseDto product;

    @Schema(title = "Warehouse place")
    WarehousePlaceDto warehousePlace;

    @Schema(title = "Quantity of product")
    Integer quantity;

}
