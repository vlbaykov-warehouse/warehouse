package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WarehousePlaceRepository extends CrudRepository<WarehousePlaceEntity, Long> {
}
