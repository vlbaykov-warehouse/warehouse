package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.service;

import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;

public interface ProductService {

    ProductEntity getById(long productId);

    ProductEntity create(ProductEntity productEntity);

    ProductEntity update(ProductEntity productEntity);

    void delete(long productId);

}
