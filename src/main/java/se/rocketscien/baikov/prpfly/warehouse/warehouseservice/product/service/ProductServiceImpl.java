package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductRepository;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public ProductEntity getById(long productId) {
        return productRepository.findById(productId).orElseThrow(() -> new ProductNotFoundException(productId));
    }

    @Override
    public ProductEntity create(ProductEntity productEntity) {
        productEntity.setId(null);
        return productRepository.save(productEntity);
    }

    @Override
    public ProductEntity update(ProductEntity productEntity) {
        ProductEntity existing = getById(productEntity.getId());
        productEntity.setVersion(existing.getVersion());
        return productRepository.save(productEntity);
    }

    @Override
    public void delete(long productId) {
        try {
            productRepository.deleteById(productId);
        } catch (EmptyResultDataAccessException e) {
            throw new ProductNotFoundException(productId);
        }
    }

}
