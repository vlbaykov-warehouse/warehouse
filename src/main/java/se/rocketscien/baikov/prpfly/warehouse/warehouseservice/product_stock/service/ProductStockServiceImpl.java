package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity_;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockEntity_;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockMoveResult;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockRepository;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.exception.NotEnoughProductInStockException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.exception.NotEnoughSpaceForProductException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse.WarehouseEntity_;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceEntity_;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceNotFoundException;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceRepository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductStockServiceImpl implements ProductStockService {

    private final ProductRepository productRepository;
    private final ProductStockRepository productStockRepository;
    private final WarehousePlaceRepository warehousePlaceRepository;

    @Override
    public ProductStockEntity putProduct(long warehousePlaceId, long productId, int quantity) {
        return changeProductQuantity(warehousePlaceId, productId, quantity);
    }

    @Override
    public ProductStockEntity takeProduct(long warehousePlaceId, long productId, int quantity) {
        return changeProductQuantity(warehousePlaceId, productId, -quantity);
    }

    @Override
    @Transactional
    public ProductStockMoveResult moveProduct(long productId, long fromWarehousePlaceId, long toWarehousePlaceId, int quantity) {
        ProductStockEntity movedFrom = takeProduct(fromWarehousePlaceId, productId, quantity);
        ProductStockEntity movedTo = putProduct(toWarehousePlaceId, productId, quantity);
        return new ProductStockMoveResult(movedFrom, movedTo);
    }

    public Page<ProductStockEntity> findProducts(String name, String city, Integer minQuantity, Integer maxQuantity, Pageable pageable) {
        Specification<ProductStockEntity> specification = getFindSpecification(name, city, minQuantity, maxQuantity);
        return productStockRepository.findAll(specification, pageable);
    }

    @Transactional
    public ProductStockEntity changeProductQuantity(long warehousePlaceId, long productId, int quantityChange) {
        WarehousePlaceEntity warehousePlaceEntity = warehousePlaceRepository.findById(warehousePlaceId)
                .orElseThrow(() -> new WarehousePlaceNotFoundException(warehousePlaceId));
        ProductEntity productEntity = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));
        ProductStockEntity entity = productStockRepository
                .findByWarehousePlaceIdAndProductId(warehousePlaceId, productId)
                .orElse(ProductStockEntity.builder().warehousePlace(warehousePlaceEntity).product(productEntity).build());
        long capacity = warehousePlaceEntity.getCapacity();
        long occupied = warehousePlaceEntity.getOccupied();
        long newOccupied = occupied + quantityChange;
        if (newOccupied > capacity) {
            throw new NotEnoughSpaceForProductException(warehousePlaceId, productId, quantityChange, (int) (capacity - occupied));
        }
        long quantity = entity.getQuantity();
        long newQuantity = quantity + quantityChange;
        if (newQuantity < 0) {
            throw new NotEnoughProductInStockException(warehousePlaceId, productId, -quantityChange, (int) quantity);
        }
        warehousePlaceEntity.setOccupied((int) newOccupied);
        entity.setWarehousePlace(warehousePlaceRepository.save(warehousePlaceEntity));
        entity.setQuantity((int) newQuantity);
        if (newQuantity == 0) {
            productStockRepository.delete(entity);
        } else {
            return productStockRepository.save(entity);
        }
        return entity;
    }

    private Specification<ProductStockEntity> getFindSpecification(String name, String city, Integer minQuantity, Integer maxQuantity) {
        return (root, query, builder) -> {
            var joinWarehousePlace = root.join(ProductStockEntity_.warehousePlace);
            var joinWarehouse = joinWarehousePlace.join(WarehousePlaceEntity_.warehouse);
            var joinProduct = root.join(ProductStockEntity_.product);
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.hasLength(name)) {
                predicates.add(builder.like(joinProduct.get(ProductEntity_.name), "%" + name + "%"));
            }
            if (StringUtils.hasLength(city)) {
                predicates.add(builder.like(joinWarehouse.get(WarehouseEntity_.city), "%" + city + "%"));
            }
            if (minQuantity != null) {
                predicates.add(builder.ge(root.get(ProductStockEntity_.quantity), minQuantity));
            }
            if (maxQuantity != null) {
                predicates.add(builder.le(root.get(ProductStockEntity_.quantity), maxQuantity));
            }
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
