package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class ProductPriceNotFoundException extends ValidationException {

    public ProductPriceNotFoundException(long productId) {
        super(HttpStatus.NOT_FOUND, "product-price.not-found", productId);
    }

}
