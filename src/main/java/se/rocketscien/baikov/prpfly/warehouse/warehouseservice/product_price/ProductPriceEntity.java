package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price;

import lombok.Getter;
import lombok.Setter;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "product_prices")
public class ProductPriceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @OneToOne
    private ProductEntity product;

    private BigDecimal amount;

    private Instant lastUpdate;

    @Override
    public String toString() {
        return "ProductPriceEntity{" +
                "id=" + id +
                ", version=" + version +
                ", productId=" + product.getId() +
                ", amount=" + amount +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
