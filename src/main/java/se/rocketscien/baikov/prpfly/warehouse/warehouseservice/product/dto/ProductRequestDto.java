package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Value
@Builder
@Jacksonized
@Schema
public class ProductRequestDto {

    @Schema(title = "Product name")
    @NotBlank(message = "product.blank-name")
    @Size(min = 3, max = 255, message = "product.invalid-name-length")
    @Pattern(regexp = "^[\\w\\s-]+$", message = "product.invalid-name-pattern")
    String name;

}
