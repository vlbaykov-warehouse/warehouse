package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;

@Value
@Builder
@Jacksonized
@Schema
public class ProductResponseDto {

    @Schema(title = "Product ID")
    Long id;

    @Schema(title = "Product name")
    String name;

    @Schema(title = "Product price")
    BigDecimal price;

    @Schema(title = "Product rating")
    BigDecimal rating;

}
