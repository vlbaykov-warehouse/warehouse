package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;

@Mapper
public interface ProductMapper {

    ProductEntity requestToEntity(ProductRequestDto request);

    ProductEntity requestToEntity(long id, ProductRequestDto request);

    @Mappings({
            @Mapping(target = "price", source = "price.amount"),
            @Mapping(target = "rating", source = "rating.amount")
    })
    ProductResponseDto entityToResponse(ProductEntity entity);

}
