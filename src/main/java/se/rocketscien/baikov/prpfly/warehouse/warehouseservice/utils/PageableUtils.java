package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.utils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class PageableUtils {

    public static Pageable getNextPageableWrapped(Page<?> currentPage) {
        if (currentPage.getTotalPages() < 1) {
            return currentPage.getPageable().withPage(0);
        }
        int nextPageNumber = (currentPage.getNumber() + 1) % currentPage.getTotalPages();
        return currentPage.getPageable().withPage(nextPageNumber);
    }

}
