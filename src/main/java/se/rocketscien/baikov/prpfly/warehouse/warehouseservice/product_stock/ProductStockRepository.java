package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductStockRepository extends JpaRepository<ProductStockEntity, Long>, JpaSpecificationExecutor<ProductStockEntity> {

    Optional<ProductStockEntity> findByWarehousePlaceIdAndProductId(long warehousePlaceId, long productId);

}
