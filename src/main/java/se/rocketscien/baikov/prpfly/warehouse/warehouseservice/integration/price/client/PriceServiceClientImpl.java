package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.price.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.PriceDto;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.WarehouseProperties;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PriceServiceClientImpl implements PriceServiceClient {

    private static final ParameterizedTypeReference<ResponseDto<PriceDto>> PRICE_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };
    private static final ParameterizedTypeReference<ResponseDto<List<PriceDto>>> PRICES_RESPONSE_TR =
            new ParameterizedTypeReference<>() {
            };

    private final RestTemplate restTemplate;
    private final WarehouseProperties properties;

    public PriceServiceClientImpl(RestTemplate restTemplate, WarehouseProperties properties) {
        this.restTemplate = restTemplate;
        this.properties = properties;
    }

    @Override
    public PriceDto getPriceByProductId(long productId) {
        String url = properties.getIntegration().getPrice().getPriceGetEndpointUrl();
        PriceDto priceDto = get(url, PRICE_RESPONSE_TR, productId);
        validatePrice(priceDto, productId);
        return priceDto;
    }

    @Override
    public List<PriceDto> getPricesByProductIds(Set<Long> productIds) {
        HttpEntity<Set<Long>> requestBody = new HttpEntity<>(productIds);
        String url = properties.getIntegration().getPrice().getPricesGetEndpointUrl();
        return post(url, requestBody, PRICES_RESPONSE_TR)
                .stream()
                .filter(priceDto -> productIds.contains(priceDto.getProductId()))
                .collect(Collectors.toList());
    }

    @SuppressWarnings("SameParameterValue")
    private <T> T get(String url, ParameterizedTypeReference<ResponseDto<T>> typeReference, Object... uriVariables) {
        return request(url, HttpMethod.GET, HttpEntity.EMPTY, typeReference, uriVariables);
    }

    @SuppressWarnings("SameParameterValue")
    private <T> T post(String url, HttpEntity<?> requestBody, ParameterizedTypeReference<ResponseDto<T>> typeReference, Object... uriVariables) {
        return request(url, HttpMethod.POST, requestBody, typeReference, uriVariables);
    }

    private <T> T request(String url, HttpMethod method, HttpEntity<?> requestBody, ParameterizedTypeReference<ResponseDto<T>> typeReference, Object... uriVariables) {
        var response = restTemplate.exchange(url, method, requestBody, typeReference, uriVariables);
        ResponseDto<T> body = response.getBody();
        if (body == null) {
            throw new IllegalStateException("Body of successful response cannot be null");
        }
        if (body.getError() != null) {
            throw new IllegalStateException(String.format("Successful response cannot contain error: [%s]", body.getError()));
        }
        T result = body.getResult();
        if (result == null) {
            throw new IllegalStateException("Result of successful response cannot be null");
        }
        return result;
    }

    private void validatePrice(PriceDto priceDto, long requiredProductId) {
        if (priceDto.getProductId() != requiredProductId) {
            throw new IllegalStateException("Fetched price is for different product than requested");
        }
    }

}
