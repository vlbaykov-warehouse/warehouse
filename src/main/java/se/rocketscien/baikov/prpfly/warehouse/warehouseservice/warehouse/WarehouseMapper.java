package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse;

import org.mapstruct.Mapper;

@Mapper
public interface WarehouseMapper {

    WarehouseDto entityToDto(WarehouseEntity entity);

}
