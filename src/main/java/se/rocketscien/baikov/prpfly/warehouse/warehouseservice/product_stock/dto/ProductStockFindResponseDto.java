package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Value
@Builder
@Jacksonized
@Schema
public class ProductStockFindResponseDto {

    @Schema(title = "Page number")
    Integer pageNumber;

    @Schema(title = "Page size")
    Integer pageSize;

    @Schema(title = "Total number of pages")
    Integer totalPages;

    @Schema(title = "Total number of elements")
    Long totalElements;

    @Schema(title = "List of elements")
    List<ProductStockDto> content;

}
