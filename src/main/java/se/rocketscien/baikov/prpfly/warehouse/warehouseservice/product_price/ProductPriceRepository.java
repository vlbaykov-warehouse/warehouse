package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface ProductPriceRepository extends CrudRepository<ProductPriceEntity, Long> {

    Optional<ProductPriceEntity> findByProductId(long productId);

    Page<ProductPriceEntity> findAllByLastUpdateLessThan(Instant since, Pageable pageable);

}
