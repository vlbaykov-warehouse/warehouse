package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class WarehousePlaceNotFoundException extends ValidationException {

    public WarehousePlaceNotFoundException(long warehousePlaceId) {
        super(HttpStatus.NOT_FOUND, "warehouse-place.not-found", warehousePlaceId);
    }

}
