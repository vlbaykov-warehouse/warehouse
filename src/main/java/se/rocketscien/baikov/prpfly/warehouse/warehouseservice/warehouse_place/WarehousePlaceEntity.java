package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place;

import lombok.Data;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse.WarehouseEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Data
@Entity
@Table(name = "warehouse_places")
public class WarehousePlaceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @ManyToOne
    private WarehouseEntity warehouse;

    private int row;

    private int shelf;

    private int capacity;

    private int occupied;

}
