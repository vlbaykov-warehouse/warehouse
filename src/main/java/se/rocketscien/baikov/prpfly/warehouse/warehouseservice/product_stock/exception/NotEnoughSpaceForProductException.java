package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.exception;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class NotEnoughSpaceForProductException extends ValidationException {

    public NotEnoughSpaceForProductException(long warehousePlaceId, long productId, int quantity, int available) {
        super(HttpStatus.UNPROCESSABLE_ENTITY,
                "product-stock.not-enough-space",
                warehousePlaceId,
                quantity,
                productId,
                available);
    }

}
