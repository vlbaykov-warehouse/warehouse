package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.dto.ProductResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.service.ProductService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/product")
@Tag(name = "Product API")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping(path = "/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get product by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
    })
    public ResponseDto<ProductResponseDto> getProduct(
            @PathVariable @Parameter(name = "Product identifier", required = true) long productId
    ) {
        return ResponseDto.result(productMapper.entityToResponse(productService.getById(productId)));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create new product")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request")
    })
    public ResponseDto<ProductResponseDto> createProduct(
            @RequestBody @NotNull @Valid ProductRequestDto productRequestDto
    ) {
        return ResponseDto.result(
                productMapper.entityToResponse(
                        productService.create(productMapper.requestToEntity(productRequestDto))
                )
        );
    }

    @PutMapping(
            path = "/{productId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Update product")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
    })
    public ResponseDto<ProductResponseDto> updateProduct(
            @PathVariable @Parameter(name = "Product identifier", required = true) long productId,
            @RequestBody @NotNull @Valid ProductRequestDto productRequestDto
    ) {
        return ResponseDto.result(
                productMapper.entityToResponse(
                        productService.update(productMapper.requestToEntity(productId, productRequestDto))
                )
        );
    }

    @DeleteMapping(path = "/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Delete product")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
    })
    public ResponseDto<?> deleteProduct(
            @PathVariable @Parameter(name = "Product identifier", required = true) long productId
    ) {
        productService.delete(productId);
        return ResponseDto.empty();
    }

}
