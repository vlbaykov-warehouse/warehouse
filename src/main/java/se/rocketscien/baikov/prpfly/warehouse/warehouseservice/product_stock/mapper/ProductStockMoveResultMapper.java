package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.mapper;

import org.mapstruct.Mapper;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockMoveResult;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveResponseDto;

@Mapper(uses = ProductStockMapper.class)
public interface ProductStockMoveResultMapper {

    ProductStockMoveResponseDto toDto(ProductStockMoveResult moveResult);

}
