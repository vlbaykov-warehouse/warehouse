package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface ProductRatingRepository extends CrudRepository<ProductRatingEntity, Long> {

    Optional<ProductRatingEntity> findByProductId(long productId);

    Page<ProductRatingEntity> findAllByLastUpdateLessThan(Instant since, Pageable pageable);

}
