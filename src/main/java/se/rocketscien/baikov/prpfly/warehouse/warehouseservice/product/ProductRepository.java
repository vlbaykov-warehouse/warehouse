package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {

    Page<ProductEntity> findAllByPriceIsNull(Pageable pageable);

    Page<ProductEntity> findAllByRatingIsNull(Pageable pageable);

}
