package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place;

import org.mapstruct.Mapper;

@Mapper
public interface WarehousePlaceMapper {

    WarehousePlaceDto entityToDto(WarehousePlaceEntity entity);

}
