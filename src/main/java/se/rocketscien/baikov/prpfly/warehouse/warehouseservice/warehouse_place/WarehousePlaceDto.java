package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse.WarehouseDto;

@Value
@Builder
@Jacksonized
@Schema
public class WarehousePlaceDto {

    @Schema(title = "Warehouse place ID")
    Long id;

    @Schema(title = "Warehouse")
    WarehouseDto warehouse;

    @Schema(title = "Row")
    Integer row;

    @Schema(title = "Shelf")
    Integer shelf;

    @Schema(title = "Capacity")
    Integer capacity;

    @Schema(title = "Occupied")
    Integer occupied;

}
