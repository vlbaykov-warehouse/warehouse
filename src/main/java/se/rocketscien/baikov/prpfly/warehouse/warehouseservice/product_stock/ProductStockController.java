package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockFindResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveRequestDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockMoveResponseDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.mapper.ProductStockMapper;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.mapper.ProductStockMoveResultMapper;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.service.ProductStockService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/stock")
@Tag(name = "Product stock API")
@RequiredArgsConstructor
@Validated
public class ProductStockController {

    private final ProductStockService productStockService;
    private final ProductStockMapper productStockMapper;
    private final ProductStockMoveResultMapper productStockMoveResultMapper;

    @PostMapping(path = "/place/{warehousePlaceId}/product/{productId}/put", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Put product to stock")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product or warehouse place not found"),
            @ApiResponse(responseCode = "422", description = "Not enough space on warehouse place"),
    })
    public ResponseDto<ProductStockDto> putProduct(
            @PathVariable long warehousePlaceId,
            @PathVariable long productId,
            @RequestParam @Min(value = 1, message = "product-stock.invalid-quantity") int quantity
    ) {
        return ResponseDto.result(
                productStockMapper.entityToDto(productStockService.putProduct(warehousePlaceId, productId, quantity))
        );
    }

    @PostMapping(path = "/place/{warehousePlaceId}/product/{productId}/take", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Take product from stock")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product or warehouse place not found"),
            @ApiResponse(responseCode = "422", description = "Not enough product on warehouse place"),
    })
    public ResponseDto<ProductStockDto> takeProduct(
            @PathVariable long warehousePlaceId,
            @PathVariable long productId,
            @RequestParam @Min(value = 1, message = "product-stock.invalid-quantity") int quantity
    ) {
        return ResponseDto.result(
                productStockMapper.entityToDto(productStockService.takeProduct(warehousePlaceId, productId, quantity))
        );
    }

    @PostMapping(
            path = "/move",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Move product")
    @ApiResponses({
            @ApiResponse(responseCode = "400", description = "Bad Request"),
            @ApiResponse(responseCode = "404", description = "Product or warehouse place not found"),
            @ApiResponse(responseCode = "422", description = "Not enough space or product on warehouse place"),
    })
    public ResponseDto<ProductStockMoveResponseDto> moveProduct(
            @RequestBody @NotNull @Valid ProductStockMoveRequestDto moveRequest
    ) {
        return ResponseDto.result(
                productStockMoveResultMapper.toDto(productStockService.moveProduct(
                        moveRequest.getProductId(),
                        moveRequest.getFromWarehousePlaceId(),
                        moveRequest.getToWarehousePlaceId(),
                        moveRequest.getQuantity()
                ))
        );
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Find products")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    public ResponseDto<ProductStockFindResponseDto> findProducts(
            @RequestParam @Nullable String name,
            @RequestParam @Nullable String city,
            @RequestParam @Nullable Integer minQuantity,
            @RequestParam @Nullable Integer maxQuantity,
            Pageable pageable
    ) {
        return ResponseDto.result(
                productStockMapper.pageToDto(
                        productStockService.findProducts(name, city, minQuantity, maxQuantity, pageable)
                )
        );
    }

}
