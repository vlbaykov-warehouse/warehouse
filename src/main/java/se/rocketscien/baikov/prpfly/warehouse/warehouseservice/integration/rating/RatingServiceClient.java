package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.integration.rating;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import se.rocketscien.baikov.prpfly.warehouse.common.dto.ResponseDto;

@FeignClient(name = "RatingServiceClient", url = "${warehouse.integration.rating.url}")
public interface RatingServiceClient {

    @RequestMapping(method = RequestMethod.POST, path = "/product/{productId}")
    ResponseDto<?> requestRatingByProductId(@PathVariable("productId") long productId);

}
