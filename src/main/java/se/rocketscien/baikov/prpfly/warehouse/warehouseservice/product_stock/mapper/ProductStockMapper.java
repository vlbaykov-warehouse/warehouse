package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.mapper;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductMapper;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.ProductStockEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockDto;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto.ProductStockFindResponseDto;

import java.util.Collection;
import java.util.List;

@Mapper(uses = ProductMapper.class)
public interface ProductStockMapper {

    ProductStockDto entityToDto(ProductStockEntity entity);

    List<ProductStockDto> entitiesToDtos(Collection<ProductStockEntity> entities);

    default ProductStockFindResponseDto pageToDto(Page<ProductStockEntity> page) {
        return ProductStockFindResponseDto.builder()
                .pageNumber(page.getNumber())
                .pageSize(page.getSize())
                .totalPages(page.getTotalPages())
                .totalElements(page.getTotalElements())
                .content(entitiesToDtos(page.getContent()))
                .build();
    }

}
