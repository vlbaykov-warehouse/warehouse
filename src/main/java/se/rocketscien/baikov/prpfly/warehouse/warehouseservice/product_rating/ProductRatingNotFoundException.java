package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class ProductRatingNotFoundException extends ValidationException {

    public ProductRatingNotFoundException(long productId) {
        super(HttpStatus.NOT_FOUND, "product-rating.not-found", productId);
    }

}
