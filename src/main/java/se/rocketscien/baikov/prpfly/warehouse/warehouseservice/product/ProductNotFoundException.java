package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product;

import org.springframework.http.HttpStatus;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.ValidationException;

public class ProductNotFoundException extends ValidationException {

    public ProductNotFoundException(long productId) {
        super(HttpStatus.NOT_FOUND, "product.not-found", productId);
    }

}
