package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import javax.validation.constraints.Min;

@Value
@Builder
@Jacksonized
@Schema
public class ProductStockMoveRequestDto {

    @Schema(title = "Product ID to move")
    Long productId;

    @Schema(title = "Warehouse place id to take product from")
    Long fromWarehousePlaceId;

    @Schema(title = "Warehouse place id to put product to")
    Long toWarehousePlaceId;

    @Schema(title = "Quantity of product to move")
    @Min(value = 1, message = "product-stock.min-move-quantity")
    Integer quantity;

}
