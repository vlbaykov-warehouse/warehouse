package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product;

import lombok.Getter;
import lombok.Setter;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_price.ProductPriceEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_rating.ProductRatingEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Getter
@Setter
@Entity
@Table(name = "products")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    private String name;

    @OneToOne
    private ProductPriceEntity price;

    @OneToOne
    private ProductRatingEntity rating;

    @Override
    public String toString() {
        Long priceId = price != null ? price.getId() : null;
        Long ratingId = rating != null ? rating.getId() : null;
        return "ProductEntity{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", priceId=" + priceId +
                ", ratingId=" + ratingId +
                '}';
    }
}
