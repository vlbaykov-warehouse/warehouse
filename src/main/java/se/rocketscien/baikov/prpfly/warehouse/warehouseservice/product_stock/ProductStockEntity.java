package se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product_stock;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.product.ProductEntity;
import se.rocketscien.baikov.prpfly.warehouse.warehouseservice.warehouse_place.WarehousePlaceEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_stocks")
public class ProductStockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @ManyToOne
    private ProductEntity product;

    @ManyToOne
    private WarehousePlaceEntity warehousePlace;

    private int quantity;

}
