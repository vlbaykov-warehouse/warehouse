https://conf.rocketscien.se/pages/viewpage.action?pageId=20483240

### Предыстория

Компания “Куркуль” известна на рынке своими чудесными складами: там всегда чистота и порядок. За это её и любят.

Сейчас учёт товаров на каждом из складов ведётся в Excel-е. Руководству хорошо понятно, что возможностями Excel-а — не справиться с грядущим китайским импортом, поэтому поездку на Мальдивы решили отложить ради автоматизации складов.

### Введение

Склад — это его название и город, оба в строковом виде. Еще у него есть складские места, каждое из которых характеризуется рядом, полкой и вместимостью.

Товары, которые сейчас есть на складах — расположены на складских местах. Для целей нашей задачи все товары имеют одинаковый размер, поэтому вместимость складского места измеряется количеством товаров. На одном складском месте может быть несколько разных товаров, но суммарное их количество не может быть больше его вместимости.

### Постановка задачи

Необходимо написать REST-приложение (**warehouse-service**) на Spring Boot, которое позволит:

- Оприходовать товар на складе — расположить его на складском месте.
- Убрать товар со склада.
- Перевезти товар со склада на склад.
- Выяснить какие товары есть на складах — их количество и расположение. Фильтры - город, название товара, количество “от” и “до”
