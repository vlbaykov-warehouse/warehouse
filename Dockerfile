FROM bellsoft/liberica-openjre-alpine:17
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java",\
    "-Xms50M","-Xmx50M",\
    "-XX:MaxMetaspaceSize=100M",\
    "-XX:MaxDirectMemorySize=1M",\
    "-XX:ReservedCodeCacheSize=30M",\
    "-jar","/app.jar"\
]